from odoo import fields, models

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'
    secondary_uom = fields.Float("Secondary UOM")
