from odoo import fields, models


class StockPicking(models.Model):
    _inherit = 'stock.picking'
    sale_order_line_id = fields.Many2one("sale.order.line")
    secondary_uom = fields.Float("Secondary UOM")
