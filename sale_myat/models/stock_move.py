from odoo import fields, models


class StockPicking(models.Model):
    _inherit = 'stock.move'

    sale_order_line_id = fields.Many2one("sale.order.line")
    secondary_uom = fields.Float("Secondary UOM", related="sale_order_line_id.secondary_uom")
