from odoo import fields, models

class SaleOrder(models.Model):
    _inherit = "sale.order"
    is_available = fields.Boolean(string='Is Available')
