from odoo import fields, models


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    secondary_uom = fields.Float("Secondary UOM")
    secondary_unit_price = fields.Float("2 Unit price")
    secondary_subtotal = fields.Float("Secondary Subtotal")
