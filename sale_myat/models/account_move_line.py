from odoo import fields, models


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'
    sale_order_line_id = fields.Many2one("sale.order.line", required=True,
                                         default=lambda self: self.env["sale.order.line"].search([]))
    # secondary_uom = fields.Float("Secondary UOM", related="sale_order_line_id.secondary_uom")
    secondary_uom = fields.Float("Secondary UOM")
    secondary_unit_price = fields.Float("2 Unit price", related="sale_order_line_id.secondary_unit_price")
    secondary_subtotal = fields.Float("Secondary Subtotal", related="sale_order_line_id.secondary_subtotal")
