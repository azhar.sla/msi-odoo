15.0.1 (19 October 2021)
-------------------------
- Intial Release

15.0.2 (25-10-2021)
-------------------
- Fix Custom Google Font Issue 

15.0.3 (13 Dec 2021)
---------------------------
-[Update] Remove All Warnings.

15.0.4 (15 Dec 2021)
------------------------
-[FIX] barcode value morthen 2 digit.(Ex. 10.9999999%) 
-[FIX] cash in/ cash out receipt design fix.
-[FIX] Order_management_screen design fix.
-[FIX] return order not working.

(17-12-2021)
--------------
- fixed "There is no space gap between quantity and UoM in the selected item of the POs in the order"
- fixed ticket screen design for mobile view

15.0.5 (21 Dec 2021)
------------------------
- [UPDATE] Add Customer Creation Offline feature
- [UPDATE] Add configuration for create coupon and redeem coupon

15.0.6 (25 Dec 2021)
------------------------
- [FIX] Fix installation time issue.

15.0.7 (29 Dec 2021)
------------------------
- [FIX] Fix issue of search product by tag.

15.0.8 (10 January 2022)
------------------------
- Compatible with pos_Coupon module
- Fix load time issue ( taking time to load )

15.0.9 (17 January 2022)
------------------------
- [ADD] Add Configuration for coupon and cash control feature.
- [FIX] Fix issue when search product.
- [ADD] Add configuration for Display suggestion product
- [FIX] Loyalty information display in receipt when Loyaltyfeature enable.
- [ADD] Add main configuration for change uom (product secondary uom) feature
- [FIX] Fix issue when send coupon by email.

15.0.10 (01 Fabruary 2022)
------------------------
- [Compatible] Make module compatible with Create Purchase Order module.
- [ADD] Add create sale order feature.
-[add] add 2 configuration for display product code in cart and receipt
-[add] make compatible stock information module.


15.0.11 (08 Feb 2022)
----------------------
-[Add] add buttons in order screen for filter-order.

